@sanity
Feature:
  Background: Customer logs in
    * def response = call read('classpath:src/test/java/testSuit/authTokenGenerator/authTokenGenerator.feature')
    * def authToken = response.response.authToken
  Scenario:
    Given url baseUri + searchCarsPath
    And header Authorization = 'Bearer ' + authToken
    When method GET
    Then status 200
    And print response
    * def id = response.available_cars[0]._id
    And print id
   #--------------------------------------------------------------------------
    Given url baseUri + "car/calculate-price/" + id +"/2021-05-23/2021-05-24"
    And header Authorization = 'Bearer ' + authToken
    When method GET
    Then status 200
    And print response