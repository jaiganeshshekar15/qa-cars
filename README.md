qa-cars 
Automating Car Rental apis using Core Java and Karate DSL Framework
 
This README would normally document whatever steps are necessary to get your application up and running.

*Installation
Clone this Repository to any IDE as a Maven Project

Command Line Run : 
	* Open any Terminal
    * Root to the project directory
    * Run the command mvn clean test
    * Runing from the TestRunner.java
	
Eclipse/IntelliJ	
	* find the TestRunner.java file from the below path from the source repository:
	* src/test/java/TestRunner.java
	* open the file
	* Right click and Run as Java Application/ Run TestRunner

Pipeline Run:
	* In qa-cars Repo
	* Go to pipeline 
	* click on Re-run button

Author
    *Jai Ganesh Shekar
*LinkedIn: www.linkedin.com/in/jai-ganesh-shekar-4569301aa 
*Gmail : jaiganeshshekar@gmail.com

